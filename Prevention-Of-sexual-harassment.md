# Prevention of Sexual Harassment

## Sexual Harassment
Sexual Harassment is any **unwelcome** verbal, visual, or physical conduct of a sexual nature that is **severe** or **pervasive** and affects working conditions or creates a hostile work environment.


## Kinds of behaviour cause sexual harassment:

Generally 3 forms of Sexual Harrassment behaviour are there

### Verbal harrassment includes:

+ Comments about clothing, a person's body, sexual, gender-based jokes, or remarks.
+ Requesting sexual favors or repeatedly asking a person out.
+ Here also includes sexual innuendos.
+ Threats
+ Spreading rumors about a person's personal, or sexual life or using foul and absent language.

### Visual harrassment includes:

+ Absence Poster, Drawings/Pictures.
+ Screensavers.
+ Cartoons.
+ Emails or texts of a sexual nature.

### Physical harrassment includes:

+ Sexual assault.
+ Impending or blocking movement.
+ Inappropriate touching such as kissing, hugging, patting, stroking, or rubbing.
+ Sexual gesturing either leering or staring.


## What would you do in case you face or witness any incident or repeated incidents of such behaviour: 

It is important to take any incidents of sexual harassment seriously and to address them promptly and effectively. If I am witness an incident of sexual harassment, I should report it to a supervisor or a designated individual or office responsible for handling complaints of harassment. This could be a human resources representative, a manager, or an employee designated to handle such complaints. If I am the victim of sexual harassment, I should also report it to a supervisor or designated individual or office.

If I am unsure how to report an incident or are not comfortable doing so, I may also be able to seek guidance and support from a trusted friend, family member, or a professional such as a therapist or counselor. It is important to remember that I have the right to work in an environment that is free from sexual harassment and that I have the right to speak up and report any incidents of harassment that is my experience or witness.