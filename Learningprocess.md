# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### Feynman Technique:

+ The Feynman Technique is a learning method that will explain a concept in a simple way to someone else, by using this technique you learn more about the topic.

+ There are some steps to do this technique choose the concept that we need to learn and try to explain it in a simple way to the other person. By comparing your explanation to the original source, We can get to know gaps in our clarity about the concept and then improve our understanding of the concept.

### Different ways to implement this technique in your learning process:

1) Take a paper and list out the concepts that you want to learn about that concepts.

2) Take another paper and write the concept name at the top of the sheet in the list wise.

3) Start to explain someone in simple words with examples like diagrammatically or mathematically and without using tricky words.

4) Find the gaps which are faced in the explanation, check the original resources do work to improve on the conceptual understanding.

## 2. Learning How to Learn TED talk by Barbara Oakley

### Video in detail:

Barbara Oakley describes the learning methodologies in her research on learning.

+ There are such keys to learning effectively, how to use those ideas in our learning process and simplifying the brain working on learning.

+ The main Barbara Oakley explained two modes and one mechanism for improving learning efficiency. The first mode is **Focus mode**  the second mode is **Diffuse mode**.

+ Barbara Oakley has explained two modes are worked based on one pinball machine to understand where we are stuck and how to overcome physical stress and get a creative solution. 

+ Using the **Pomodoro technique** helps to manage the focus on learning and relaxation.

+ Slower thinkers need to put hard work into learning, but they are very clear about every learning process, slower thinkers are deep learners.

+ Illusion competencies in learning are overcome by revising and practicing more.

### Some of the steps that you can take to improve your learning process:

+ Improve the learning process  concentration on learning by using focus mode  this time we are tightly finding on thoughts and get some where stuck  should needs to allow diffuse.

+ This diffuse mode will give mind relaxation, helps to get new thoughts will find some creative path.

+ Using the Pomodoro technique will improve our learning process technique will continue focused on learning for some time and relaxing for some time will help to control physical and mental relaxation.

## 3. Learn Anything in 20 hours

### Key takeaways from the video:

+ Find to learn something by the focus on that particular skill or concept and trying to understand only one topic at a time for that scheduled time slot.

+ Deconstruct the skill into perfect parts and avoid learning distractions from the concept.

+ If any stuck happens on do not needs to waste our time, do research and approach the perfect resource and get to find, wasting time on only one topic let's say wasting 10000 hr are not worked anymore.

+ Learning a new concept or skill for 20 hours is enough just to be focused.

+ Take a break for some time and keep practicing on that particular topic you will improve your skill.

### Some of the steps that you can while approaching a new topic:

+ Note the topic and deconstruct the concept in small parts to learn one by one.

+ Find where you are stuck and practice on that, don't waste your time for long.

+ Make a timetable for the concept completion and do follow the schedule.

+ Take a break and relax for some time start time to complete the concept in a better way.